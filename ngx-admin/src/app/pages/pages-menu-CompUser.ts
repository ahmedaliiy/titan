import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS_COMPANYUSER: NbMenuItem[] = [
  {
    title: 'Employees',
    icon: 'globe-outline',
    link: '/pages/employees/list',
    home: true,
  }
];