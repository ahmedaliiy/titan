import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { AuthGuard } from '../helpers/auth-guard.service';
import { Role } from './ConstsRoles';

const routes: Routes = [{
  path: '',
  canActivate: [AuthGuard], 
  component: PagesComponent,
  children: [
    {
      path: 'countries',
      canActivate: [AuthGuard], 
      data: { roles: [Role.Admin] } ,
      loadChildren: () => import('./countries/countries.module')
      .then(m => m.CountriesModule),
    },
    {
      path: 'languages',
      canActivate: [AuthGuard], 
      data: { roles: [Role.Admin] } ,
      loadChildren: () => import('./languages/languages.module')
      .then(m => m.LanguagesModule),
    },
    {
      path: 'salutations',
      canActivate: [AuthGuard], 
      data: { roles: [Role.Admin] } ,
      loadChildren: () => import('./salutations/salutations.module')
      .then(m => m.SalutationsModule),
    },
    {
      path: 'municipalities',
      canActivate: [AuthGuard], 
      data: { roles: [Role.Admin] } ,
      loadChildren: () => import('./municipalities/municipalities.module')
      .then(m => m.MunicipalitiesModule),
    },
    {
      path: 'users',
      canActivate: [AuthGuard], 
      data: { roles: [Role.Admin] } ,
      loadChildren: () => import('./users/users.module')
      .then(m => m.UsersModule),
    },
    {
      path: 'companies',
      canActivate: [AuthGuard], 
      data: { roles: [Role.Admin,Role.Auditor] } ,
      loadChildren: () => import('./companies/companies.module')
      .then(m => m.CompaniesModule),
    },
    {
      path: 'employees',
      canActivate: [AuthGuard], 
      data: { roles: [Role.Admin,Role.Auditor,Role.CompanyUser] } ,
      loadChildren: () => import('./employees/employees.module')
      .then(m => m.EmployeesModule),
    },
    {
      path: '',
      redirectTo: 'employees/list',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
