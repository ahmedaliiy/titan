import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Municipality } from '../municipalities.model';
import { NgForm } from '@angular/forms';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { MunicipalityService } from '../../../services/municipalityService.service';

@Component({
  selector: 'ngx-municipality-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {
  municipality: Municipality;
  id = this.route.snapshot.params['id'];

  constructor(private municipalityService: MunicipalityService, private route: ActivatedRoute, private router: Router, private toastrService: NbToastrService) { }

  ngOnInit(): void {
    this.municipality = new Municipality();
    this.municipalityService.getMunicipalityById(this.id)
    .then(res=> this.municipality = res as Municipality);
  }
  
  updateMunicipality(f:NgForm){
    this.municipalityService.putMunicipality(f.value).subscribe(res => {
      this.toastrService.show("Update Successfully" ,"Municipality Update",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.resetForm(f);
      this.router.navigate(['/pages/municipalities/list']);
    },(err: any)=>{
      this.toastrService.show("Something wrong" ,"Municipality Update",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    })
  }

  resetForm(f?:NgForm){
    if(f != null)
      f.resetForm();
    this.municipalityService.formData = {
      id: null,
      name: ''
    }
  }
}
