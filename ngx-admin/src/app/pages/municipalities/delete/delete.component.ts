import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbToastrService, NbGlobalPhysicalPosition } from '@nebular/theme';
import { MunicipalityService } from '../../../services/municipalityService.service';

@Component({
  selector: 'ngx-municipality-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {

  constructor(private municipalityService: MunicipalityService, private route: ActivatedRoute, private router: Router,private toastrService: NbToastrService) { }

  ngOnInit(): void {
  }

  delete(){
    this.municipalityService.deleteMunicipality(this.route.snapshot.params['id']).subscribe( res =>{
      this.toastrService.show("Delete Successfully" ,"Municipality Delete",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.router.navigate(['/pages/municipalities/list']);
    },(err: any)=>{
      this.toastrService.show("Something wrong" ,"Municipality Delete",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    });  
  }
}
