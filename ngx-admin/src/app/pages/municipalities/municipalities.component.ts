import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-municipalities',
  template: `<router-outlet></router-outlet>`
})
export class MunicipalitiesComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
    this.router.navigate(['/pages/municipalities/list']);
  }

}
