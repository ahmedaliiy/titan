import { Component, OnInit } from '@angular/core';
import { Municipality } from '../municipalities.model';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { MunicipalityService } from '../../../services/municipalityService.service';

@Component({
  selector: 'ngx-municipality-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  municipalities: Municipality[];

  constructor(private http: HttpClient,private router: Router,private municipalityService: MunicipalityService) { }

  ngOnInit(): void {
    this.municipalityService.refreshList().then(res => this.municipalities = res as Municipality[]);
  }

  navigate(type,id?:number) {
    if(type=='add'){
    this.router.navigate(['/pages/municipalities/add']);
    }
    if(type=='edit'){
      this.router.navigate(['/pages/municipalities/update/'+id]);
      }
      if(type=='delete'){
        this.router.navigate(['/pages/municipalities/delete/'+id]);
        }
  }
}
