import { Component, OnInit } from '@angular/core';
import { Municipality } from '../municipalities.model';
import { NgForm } from '@angular/forms';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';
import { MunicipalityService } from '../../../services/municipalityService.service';

@Component({
  selector: 'ngx-municipality-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  municipality: Municipality;
  
  constructor(private router: Router,private municipalityService: MunicipalityService,private toastrService: NbToastrService) { }

  ngOnInit(): void {
    this.municipality = new Municipality();
  }

  addMunicipality(f: NgForm){
    this.municipalityService.postMunicipality(f.value).subscribe(res => {
      this.toastrService.show("Insert Successfully" ,"Municipality Register",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.resetForm(f);
      this.router.navigate(['/pages/municipalities/list']);
    },(err: any)=>{
      this.toastrService.show("Something wrong" ,"Municipality Register",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    })
  }

  resetForm(f?:NgForm){
    if(f != null)
      f.resetForm();
    this.municipalityService.formData = {
      id: null,
      name: ''
    }
  }
}
