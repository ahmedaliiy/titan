import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MunicipalitiesComponent } from './municipalities.component';
import { AddComponent } from './add/add.component';
import { UpdateComponent } from './update/update.component';
import { DeleteComponent } from './delete/delete.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [{
  path: '',
  component: MunicipalitiesComponent,
  children: [
    {
      path: 'add',
      component: AddComponent,
    },
    {
      path: 'update/:id',
      component: UpdateComponent,
    },
    {
        path: 'delete/:id',
        component: DeleteComponent,
    },
    {
      path: 'list',
      component: ListComponent,
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MunicipalitiesRoutingModule { }

export const routedComponents = [
  MunicipalitiesComponent,
  AddComponent,
  UpdateComponent,
  DeleteComponent,
  ListComponent
];
