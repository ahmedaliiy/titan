import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Salutation } from '../salutations.model';
import { NgForm } from '@angular/forms';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { SalutationService } from '../../../services/salutationService.service';

@Component({
  selector: 'ngx-salutation-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {
  salutation: Salutation;
  id = this.route.snapshot.params['id'];

  constructor(private salutationService: SalutationService, private route: ActivatedRoute, private router: Router, private toastrService: NbToastrService) { }

  ngOnInit(): void {
    this.salutation = new Salutation();
    this.salutationService.getSalutationById(this.id)
    .then(res=> this.salutation = res as Salutation);
  }

  updateSalutation(f:NgForm){
    this.salutationService.putSalutation(f.value).subscribe(res => {
      this.toastrService.show("Update Successfully" ,"Salutation Update",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.resetForm(f);
      this.router.navigate(['/pages/salutations/list']);
    },(err: any)=>{
      this.toastrService.show("Something wrong" ,"Salutation Update",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    })
  }

  resetForm(f?:NgForm){
    if(f != null)
      f.resetForm();
    this.salutationService.formData = {
      id: null,
      name: ''
    }
  }
}
