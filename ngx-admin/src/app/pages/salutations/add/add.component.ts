import { Component, OnInit } from '@angular/core';
import { Salutation } from '../salutations.model';
import { NgForm } from '@angular/forms';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';
import { SalutationService } from '../../../services/salutationService.service';

@Component({
  selector: 'ngx-salutation-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  salutation: Salutation;

  constructor(private router: Router,private salutationService: SalutationService,private toastrService: NbToastrService) { }

  ngOnInit(): void {
    this.salutation = new Salutation();
  }

  addSalutation(f:NgForm){
    this.salutationService.postSalutation(f.value).subscribe(res => {
      this.toastrService.show("Insert Successfully" ,"Salutation Register",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.resetForm(f);
      this.router.navigate(['/pages/salutations/list']);
    },(err: any)=>{
      this.toastrService.show("Something wrong" ,"Salutation Register",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    })
  }

  resetForm(f?:NgForm){
    if(f != null)
      f.resetForm();
    this.salutationService.formData = {
      id: null,
      name: ''
    }
  }
}
