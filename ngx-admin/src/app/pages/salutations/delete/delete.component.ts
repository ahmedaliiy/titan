import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbToastrService, NbGlobalPhysicalPosition } from '@nebular/theme';
import { SalutationService } from '../../../services/salutationService.service';

@Component({
  selector: 'ngx-salutation-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {

  constructor(private salutationService:SalutationService, private route: ActivatedRoute, private router: Router,private toastrService: NbToastrService) { }

  ngOnInit(): void {
  }

  delete(){
    this.salutationService.deleteSalutation(this.route.snapshot.params['id']).subscribe( res =>{
      this.toastrService.show("Delete Successfully" ,"Salutation Delete",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.router.navigate(['/pages/salutations/list']);
    },(err: any)=>{
      this.toastrService.show("Something wrong" ,"Salutation Delete",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    });  
  }
}
