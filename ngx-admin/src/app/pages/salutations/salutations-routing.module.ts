import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SalutationsComponent } from './salutations.component';
import { AddComponent } from './add/add.component';
import { UpdateComponent } from './update/update.component';
import { DeleteComponent } from './delete/delete.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [{
  path: '',
  component: SalutationsComponent,
  children: [
    {
      path: 'add',
      component: AddComponent,
    },
    {
      path: 'update/:id',
      component: UpdateComponent,
    },
    {
        path: 'delete/:id',
        component: DeleteComponent,
    },
    {
      path: 'list',
      component: ListComponent,
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SalutationsRoutingModule { }

export const routedComponents = [
  SalutationsComponent,
  AddComponent,
  UpdateComponent,
  DeleteComponent,
  ListComponent
];
