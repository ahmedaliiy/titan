import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-salutations',
  template: `<router-outlet></router-outlet>`
})
export class SalutationsComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.router.navigate(['/pages/salutations/list']);
  }

}
