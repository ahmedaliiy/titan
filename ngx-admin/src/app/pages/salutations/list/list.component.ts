import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Salutation } from '../salutations.model';
import { HttpClient } from '@angular/common/http';
import { SalutationService } from '../../../services/salutationService.service';

@Component({
  selector: 'ngx-salutation-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  salutations: Salutation[];

  constructor(private http: HttpClient,private router: Router,private SalutationService: SalutationService) { }

  ngOnInit(): void {
    this.SalutationService.refreshList().then(res => this.salutations = res as Salutation[]);
  }

  navigate(type,id?:number) {
    if(type=='add'){
    this.router.navigate(['/pages/salutations/add']);
    }
    if(type=='edit'){
      this.router.navigate(['/pages/salutations/update/'+id]);
      }
      if(type=='delete'){
        this.router.navigate(['/pages/salutations/delete/'+id]);
        }
  }
}
