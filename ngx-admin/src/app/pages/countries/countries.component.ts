import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-countries',
  template: `<router-outlet></router-outlet>`
})
export class CountriesComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.router.navigate(['/pages/countries/list']);
  }

}
