import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Country } from '../country.model';
import { NgForm } from '@angular/forms';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { CountryService } from '../../../services/countryService.service';

@Component({
  selector: 'ngx-country-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {
  country: Country;
  id = this.route.snapshot.params['id'];

  constructor(private countryService: CountryService, private route: ActivatedRoute, private router: Router, private toastrService: NbToastrService) { }

  ngOnInit(): void {
    this.country = new Country();
    this.countryService.getCountryById(this.id)
    .then(res=> this.country = res as Country);
  }

  updateCountry(f:NgForm){
    this.countryService.putCountry(f.value).subscribe(res => {
      this.toastrService.show("Update Successfully" ,"Country Update",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.resetForm(f);
      this.router.navigate(['/pages/countries/list']);
    },(err: any)=>{
      this.toastrService.show("Something wrong" ,"Country Update",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    })
  }

  resetForm(f?:NgForm){
    if(f != null)
      f.resetForm();
    this.countryService.formData = {
      id: null,
      name: ''
    }
  }
}
