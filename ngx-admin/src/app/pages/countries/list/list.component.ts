import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { Country } from '../country.model';
import { CountryService } from '../../../services/countryService.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ngx-country-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  countries: Country[];
  
  constructor(private http: HttpClient,private router: Router,private countryService: CountryService) { }

  ngOnInit(): void {
    this.countryService.refreshList().then(res => this.countries = res as Country[]);
  }
  
  navigate(type,id?:number) {
    if(type=='add'){
    this.router.navigate(['/pages/countries/add']);
    }
    if(type=='edit'){
      this.router.navigate(['/pages/countries/update/'+id]);
      }
      if(type=='delete'){
        this.router.navigate(['/pages/countries/delete/'+id]);
        }
  }
}
