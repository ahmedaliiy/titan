import { Component, OnInit } from '@angular/core';
import { Country } from '../country.model';
import { NgForm } from '@angular/forms';
import { CountryService } from '../../../services/countryService.service';
import { NbToastrService, NbComponentStatus, NbGlobalPhysicalPosition } from '@nebular/theme';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-country-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  country: Country;

  constructor(private router: Router,private countryService: CountryService,private toastrService: NbToastrService) { }

  ngOnInit(): void {
    this.country = new Country();
    this.resetForm();
  }

  addCountry(f:NgForm){
    this.countryService.postCountry(f.value).subscribe(res => {
      this.toastrService.show("Insert Successfully" ,"Country Register",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.resetForm(f);
      this.router.navigate(['/pages/countries/list']);
    },(err: any)=>{
      this.toastrService.show("Something wrong" ,"Country Register",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    })
  }

  resetForm(f?:NgForm){
    if(f != null)
      f.resetForm();
    this.countryService.formData = {
      id: null,
      name: ''
    }
  }
  }