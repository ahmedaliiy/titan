import { Component, OnInit } from '@angular/core';

import { MENU_ITEMS_ADMIN } from './pages-menu-Admin';
import { MENU_ITEMS_AUDITOR } from './pages-menu-Auditor';
import { MENU_ITEMS_COMPANYUSER } from './pages-menu-CompUser';

import { AuthenticationService } from '../services/authentication.service';
import { LoginUser } from '../auth/loginUser.model';
import { Role } from './ConstsRoles';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent implements OnInit {

  constructor(private authenticationService: AuthenticationService){}

  user:LoginUser;

  ngOnInit(){
    this.user=this.authenticationService.currentUserValue;
    if(this.user.roleName==Role.Admin){
      this.menu = MENU_ITEMS_ADMIN
    }
    if(this.user.roleName==Role.Auditor){
      this.menu = MENU_ITEMS_AUDITOR
    }
    if(this.user.roleName==Role.CompanyUser){
      this.menu = MENU_ITEMS_COMPANYUSER
    }
  }
  
  menu;
}
