import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS_AUDITOR: NbMenuItem[] = [
  {
    title: 'Companies',
    icon: 'globe-outline',
    link: '/pages/companies/list',
    home: true,
  },
  {
    title: 'Employees',
    icon: 'globe-outline',
    link: '/pages/employees/list',
  }
];