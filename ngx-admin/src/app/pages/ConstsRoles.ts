
export enum Role {
    Admin = 'Admin',
    Auditor = 'Auditor',
    CompanyUser = 'CompanyUser'
  }