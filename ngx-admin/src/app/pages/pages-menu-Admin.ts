import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS_ADMIN: NbMenuItem[] = [
  {
    title: 'Definitions',
    icon: 'globe-outline',
    children:[
    {
    title: 'Countries',
    link: '/pages/countries/list',
    },
    {
      title: 'Languages',
      link: '/pages/languages/list',
    },
    {
      title: 'Salutations',
      link: '/pages/salutations/list',
    },
    {
      title: 'Municipalities',
      link: '/pages/municipalities/list',
    }]
  },
  {
    title: 'Users',
    icon: 'globe-outline',
    link: '/pages/users/list',
  },
  {
    title: 'Companies',
    icon: 'globe-outline',
    link: '/pages/companies/list',
  },
  {
    title: 'Employees',
    icon: 'globe-outline',
    link: '/pages/employees/list',
  }
];