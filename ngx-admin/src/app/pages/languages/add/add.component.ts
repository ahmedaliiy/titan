import { Component, OnInit } from '@angular/core';
import { Language } from '../language.model';
import { NgForm } from '@angular/forms';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';
import { LanguageService } from '../../../services/languageService.service';

@Component({
  selector: 'ngx-language-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  language: Language;

  constructor(private router: Router,private languageService: LanguageService,private toastrService: NbToastrService) { }

  ngOnInit(): void {
    this.language = new Language();
  }

  addLanguage(f:NgForm){
    this.languageService.postLanguage(f.value).subscribe(res => {
      this.toastrService.show("Insert Successfully" ,"Language Register",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.resetForm(f);
      this.router.navigate(['/pages/languages/list']);
    },(err: any)=>{
      this.toastrService.show("Something wrong" ,"Language Register",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    })
  }

  resetForm(f?:NgForm){
    if(f != null)
      f.resetForm();
    this.languageService.formData = {
      id: null,
      name: ''
    }
  }
}
