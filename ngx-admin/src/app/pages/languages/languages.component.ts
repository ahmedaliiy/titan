import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-languages',
  template: `<router-outlet></router-outlet>`
})
export class LanguagesComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.router.navigate(['/pages/languages/list']);
  }

}
