import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Language } from '../language.model';
import { NgForm } from '@angular/forms';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { LanguageService } from '../../../services/languageService.service';

@Component({
  selector: 'ngx-language-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {
  language: Language;
  id = this.route.snapshot.params['id'];

  constructor(private languageService: LanguageService, private route: ActivatedRoute, private router: Router, private toastrService: NbToastrService) { }

  ngOnInit(): void {
    this.language = new Language();
    this.languageService.getLanguageById(this.id)
    .then(res=> this.language = res as Language);
  }

  updateLanguage(f: NgForm){
    this.languageService.putLanguage(f.value).subscribe(res => {
      this.toastrService.show("Update Successfully" ,"Language Update",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.resetForm(f);
      this.router.navigate(['/pages/languages/list']);
    },(err: any)=>{
      this.toastrService.show("Something wrong" ,"Language Update",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    })
  }

  resetForm(f?:NgForm){
    if(f != null)
      f.resetForm();
    this.languageService.formData = {
      id: null,
      name: ''
    }
  }
}
