import { Component, OnInit } from '@angular/core';
import { Language } from '../language.model';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { LanguageService } from '../../../services/languageService.service';

@Component({
  selector: 'ngx-language-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  languages: Language[];

  constructor(private http: HttpClient,private router: Router,private languageService: LanguageService) { }

  ngOnInit(): void {
    this.languageService.refreshList().then(res => this.languages = res as Language[]);
  }

  navigate(type,id?: number) {
    if(type=='add'){
    this.router.navigate(['/pages/languages/add']);
    }
    if(type=='edit'){
      this.router.navigate(['/pages/languages/update/'+id]);
      }
      if(type=='delete'){
        this.router.navigate(['/pages/languages/delete/'+id]);
        }
  }

}
