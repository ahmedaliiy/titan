import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { LanguageService } from '../../../services/languageService.service';

@Component({
  selector: 'ngx-language-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {

  constructor(private languageService:LanguageService, private route: ActivatedRoute, private router: Router,private toastrService: NbToastrService) { }

  ngOnInit(): void {
  }

  delete(){
    this.languageService.deleteLanguage(this.route.snapshot.params['id']).subscribe( res =>{
      this.toastrService.show("Delete Successfully" ,"Language Delete",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.router.navigate(['/pages/languages/list']);
    },(err: any)=>{
      this.toastrService.show("Something wrong" ,"Language Delete",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    });  
  }
}
