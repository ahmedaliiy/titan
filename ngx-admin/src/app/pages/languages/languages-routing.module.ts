import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LanguagesComponent } from './languages.component';
import { AddComponent } from './add/add.component';
import { UpdateComponent } from './update/update.component';
import { DeleteComponent } from './delete/delete.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [{
  path: '',
  component: LanguagesComponent,
  children: [
    {
      path: 'add',
      component: AddComponent,
    },
    {
      path: 'update/:id',
      component: UpdateComponent,
    },
    {
        path: 'delete/:id',
        component: DeleteComponent,
    },
    {
      path: 'list',
      component: ListComponent,
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LanguagesRoutingModule { }

export const routedComponents = [
  LanguagesComponent,
  AddComponent,
  UpdateComponent,
  DeleteComponent,
  ListComponent
];
