import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { EmployeeService } from '../../../services/employeeService.service';

@Component({
  selector: 'ngx-employee-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {

  constructor(private employeeService:EmployeeService, private route: ActivatedRoute, private router: Router,private toastrService: NbToastrService) { }

  ngOnInit(): void {
  }

  delete(){
    this.employeeService.deleteEmployee(this.route.snapshot.params['id']).subscribe( res =>{
      this.toastrService.show("Delete Successfully" ,"Employee Delete",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.router.navigate(['/pages/employees/list']);
    },(err: any)=>{
      this.toastrService.show("Something wrong" ,"Employee Delete",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    });  
  }
}
