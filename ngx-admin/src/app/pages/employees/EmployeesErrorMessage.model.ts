export class EmployeesErrorMessage {
    firstName: string;
    lastName: string;
    code: number;
    errorMessage: string;
  }