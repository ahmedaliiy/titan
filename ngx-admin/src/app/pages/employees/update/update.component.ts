import { Component, OnInit } from '@angular/core';
import { Language } from '../../languages/language.model';
import { Country } from '../../countries/country.model';
import { Municipality } from '../../municipalities/municipalities.model';
import { Salutation } from '../../salutations/salutations.model';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Employee } from '../employee.model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { EmployeeService } from '../../../services/employeeService.service';
import { CountryService } from '../../../services/countryService.service';
import { SalutationService } from '../../../services/salutationService.service';
import { LanguageService } from '../../../services/languageService.service';
import { MunicipalityService } from '../../../services/municipalityService.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'ngx-employee-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {
  employee: Employee;
  id = this.route.snapshot.params['id'];

  editEmployee(f:NgForm){
    this.employeeService.putEmployee(f.value).subscribe(res => {
      this.toastrService.show("Update Successfully" ,"Employee Edit",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.resetForm(f);
      this.router.navigate(['/pages/employees/list']);
    },(err: any)=>{
      this.toastrService.show("Something wrong" ,"Employee Edit",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    })
  }

  constructor(private route:ActivatedRoute,private router: Router,private employeeService: EmployeeService,private toastrService: NbToastrService,
    private countryService: CountryService,private salutationService: SalutationService,private languageService: LanguageService,
    private municipalityService: MunicipalityService,public datepipe: DatePipe) { }

  ngOnInit(): void {
    this.employee = new Employee();
    this.employeeService.getEmployeeById(this.id)
    .then(res=> this.employee = res as Employee);
    this.datepipe.transform(this.employee.dateOfBirth, 'dd/MM/yyyy');      
    this.resetForm();
    this.countryService.refreshList().then(res => this.countries = res as Country[]);
    this.languageService.refreshList().then(res => this.languages = res as Language[]);
    this.salutationService.refreshList().then(res => this.salutations = res as Salutation[]);
    this.municipalityService.refreshList().then(res => this.municipalities = res as Municipality[]);
  }

  languages: Language[];

  countries: Country[];

  municipalities: Municipality[];

  salutations: Salutation[];

  resetForm(f?:NgForm){
    if(f != null)
      f.resetForm();
    this.employeeService.formData = {
      id: null,
      address1:'',
      address2: '',
      code:0,
      companyId:0,
      countryId:0,
      dateOfBirth:null,
      electronicDispatch:'',
      email:'',
      firstName:'',
      gender:0,
      languageId:0 ,
      lastName:'',
      lastSalary:0,
      municipalityId:0,
      salutationId:0,
      socialSecurityNo:'',
      lastSync:null
    }
}

}
