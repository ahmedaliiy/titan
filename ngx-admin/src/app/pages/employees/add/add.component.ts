import { Component, OnInit, Input } from '@angular/core';
import { Language } from '../../languages/language.model';
import { Country } from '../../countries/country.model';
import { Municipality } from '../../municipalities/municipalities.model';
import { Salutation } from '../../salutations/salutations.model';
import { Employee } from '../employee.model';
import { NgForm } from '@angular/forms';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { EmployeeService } from '../../../services/employeeService.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CountryService } from '../../../services/countryService.service';
import { SalutationService } from '../../../services/salutationService.service';
import { LanguageService } from '../../../services/languageService.service';
import { MunicipalityService } from '../../../services/municipalityService.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'ngx-employee-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  employee: Employee;
  companyId: number;

  constructor(private router: Router,private employeeService: EmployeeService,private toastrService: NbToastrService,
    private countryService: CountryService,private salutationService: SalutationService,private languageService: LanguageService,
    private municipalityService: MunicipalityService,public datepipe: DatePipe,private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.employee = new Employee();
    this.resetForm();
    this.countryService.refreshList().then(res => this.countries = res as Country[]);
    this.languageService.refreshList().then(res => this.languages = res as Language[]);
    this.salutationService.refreshList().then(res => this.salutations = res as Salutation[]);
    this.municipalityService.refreshList().then(res => this.municipalities = res as Municipality[]);
    this.companyId = this.route.snapshot.params['compId'];
  }

  languages: Language[];

  countries: Country[];

  municipalities: Municipality[];

  salutations: Salutation[];

  addEmployee(f: NgForm){
    this.datepipe.transform(f.value.dateOfBirth, 'dd/MM/yyyy');
    f.value.companyId = this.companyId; 
    this.employeeService.postEmployee(f.value).subscribe(res => {
      this.toastrService.show("Insert Successfully" ,"Employee Register",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.resetForm(f);
      this.router.navigate(['/pages/employees/list']);
    },(err: any)=>{
      this.toastrService.show("Something wrong" ,"Employee Register",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    })
  }

  resetForm(f?:NgForm){
    if(f != null)
      f.resetForm();
    this.employeeService.formData = {
      id: null,
      address1:'',
      address2: '',
      code:0,
      companyId:0,
      countryId:0,
      dateOfBirth:null,
      electronicDispatch:'',
      email:'',
      firstName:'',
      gender:0,
      languageId:0 ,
      lastName:'',
      lastSalary:0,
      municipalityId:0,
      salutationId:0,
      socialSecurityNo:'',
      lastSync: null
    }
}
}