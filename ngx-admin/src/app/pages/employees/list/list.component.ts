import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Employee } from '../employee.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { EmployeeService } from '../../../services/employeeService.service';
import { Company } from '../../companies/company.model';
import { CompanyService } from '../../../services/companyService.service';
import { AuthenticationService } from '../../../services/authentication.service';
import { NbToastrService, NbGlobalPhysicalPosition } from '@nebular/theme';
import { DomSanitizer } from '@angular/platform-browser';
import { ImportModel } from '../import.model';
import { EmployeesErrorMessage } from '../EmployeesErrorMessage.model';

@Component({
  selector: 'ngx-employee-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  employees: Employee[] = [];
  companies: Company[] = [];
  choosenCompany: number;
  id = this.route.snapshot.params['id'];
  isAuditor: boolean;
  isCompanyUser: boolean;
  fileUrl = '';
  importModel: ImportModel;
  pageLoaded: boolean = false;
  employeesErrorMessageList: EmployeesErrorMessage[] = [
    { firstName: 'Ahmed', lastName: 'Ali', code: 123, errorMessage: 'Duplicate' },
    { firstName: 'Ahmed', lastName: 'Ali', code: 123, errorMessage: 'Duplicate2' },
  ];
  showModal: boolean = false;

  constructor(
    private toastrService: NbToastrService,
    private ts: NbToastrService,
    private as: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router,
    private employeeService: EmployeeService,
    private companiesService: CompanyService) {}

  async ngOnInit() {
    this.importModel = {
      companyId: 0,
      file: {
        file: null,
        formFile: null,
        name: '',
      },
    };
    this.downloadFile();
    if (this.as.isAllowed == false) {
      this.ts.show('You are not allowed to enter this page', 'Security Alert', {
        status: 'danger',
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
      });
      this.as.isAllowed = true;
    }

    this.isAuditor =
      this.as.currentUserValue.roleName == 'Auditor' ? true : false;
    this.isCompanyUser =
      this.as.currentUserValue.roleName == 'CompanyUser' ? true : false;

    if (this.id == null) {
      await this.companiesService.GetActiveCompanies().then(
        (res) => (this.companies = res as Company[]),
        (err) => (this.companies = []),
      );
      this.choosenCompany = this.companies[this.companies.length - 1].id;
    } else {
      await this.companiesService.getCompanyById(this.id).then(
        (res) => {
          this.companies.push(res as Company);
          this.choosenCompany = this.id;
        },
        (err) => (this.companies = []),
      );
    }
    await this.employeeService.getEmployeeByCompany(this.choosenCompany).then(
      (res) => {
        this.employees = res as Employee[];
      },
      (err) => (this.employees = []),
    );

    this.pageLoaded = true;
  }

  changeCompany() {
    this.employeeService
      .getEmployeeByCompany(this.choosenCompany)
      .then((res) => {
        this.employees = res as Employee[];
      });
  }

  sync(id) {
    this.employeeService.syncEmployee(id).subscribe(
      (res) => {
        this.toastrService.show('Synced Successfully', 'Employee Sync', {
          status: 'success',
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
        });
      },
      (err) => {
        this.toastrService.show('Something wrong', 'Employee Sync', {
          status: 'danger',
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
        });
      },
    );
  }

  navigate(type, id?: number) {
    if (type == 'add') {
      this.router.navigate(['/pages/employees/add/' + this.choosenCompany]);
    }
    if (type == 'edit') {
      this.router.navigate(['/pages/employees/update/' + id]);
    }
    if (type == 'delete') {
      this.router.navigate(['/pages/employees/delete/' + id]);
    }
    if (type == 'show') {
      this.router.navigate(['/pages/employees/show/' + id]);
    }
  }

  downloadFile() {
    this.employeeService
      .downloadTemplate()
      .subscribe((res) => (this.fileUrl = res as string));
  }

  async handleInputChange(e) {
    let files = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    let reader = new FileReader();
    reader.onload = await this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(files);
    this.importModel.file.name = files.name;
  }
  _handleReaderLoaded(e) {
    const reader = e.target;
    this.importModel.file.file = reader.result;
  }

 uploadFiles() {
    if (this.importModel.file.file == null)
      this.toastrService.show('Select File First', 'Import File', {
        status: 'danger',
        position: NbGlobalPhysicalPosition.TOP_RIGHT,
      });
    else {
      this.importModel.companyId = this.choosenCompany;

     this.employeeService.importFile(this.importModel).then((res) => {
         this.employeesErrorMessageList = res as EmployeesErrorMessage[];
         
        if (this.employeesErrorMessageList.length == 0){
          this.showModal = false;
          this.choosenCompany = this.importModel.companyId;
          this.changeCompany();
          this.toastrService.show('Uploaded Successfully', 'Import File', {
            status: 'success',
            position: NbGlobalPhysicalPosition.TOP_RIGHT,
          });
        }
        else
        {
          for(let i =0;i<this.employeesErrorMessageList.length;i++){
            this.employeesErrorMessageList[i].errorMessage = this.employeesErrorMessageList[i].errorMessage.substring(1).
          replace(/,/g, ' - ');
          }
          this.showModal = true;
        }
      }, (err) => {
        this.toastrService.show('Something Wrong', 'Import File', {
          status: 'danger',
          position: NbGlobalPhysicalPosition.TOP_RIGHT,
        });
      });
    }
  }
}
