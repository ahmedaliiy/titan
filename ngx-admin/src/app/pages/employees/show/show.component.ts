import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication.service';
import { LoginUser } from '../../../auth/loginUser.model';
import { Role } from '../../ConstsRoles';
import { Employee } from '../employee.model';
import { EmployeeService } from '../../../services/employeeService.service';
import { CompanyService } from '../../../services/companyService.service';
import { Company } from '../../companies/company.model';
import { CountryService } from '../../../services/countryService.service';
import { LanguageService } from '../../../services/languageService.service';
import { SalutationService } from '../../../services/salutationService.service';
import { MunicipalityService } from '../../../services/municipalityService.service';
import { Country } from '../../countries/country.model';
import { Language } from '../../languages/language.model';
import { Salutation } from '../../salutations/salutations.model';
import { Municipality } from '../../municipalities/municipalities.model';
import { SalutationsRoutingModule } from '../../salutations/salutations-routing.module';

@Component({
  selector: 'ngx-employee-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.scss']
})
export class ShowComponent implements OnInit {
  employee: Employee;
  id = this.route.snapshot.params['id'];
  loggedUser: LoginUser;
  isAdmin: boolean;
  gender: string;
  
  constructor(private authenticationService: AuthenticationService,private employeeService: EmployeeService, private route: ActivatedRoute, private router: Router) { }

  async ngOnInit() {
    this.employee = new Employee();
    await this.employeeService.getEmployeeById(this.id)
    .then(res=> {
      this.employee = res as Employee;
    });

    this.gender = (this.employee.gender == 1) ? 'Male' : 'Female';

    this.loggedUser = new LoginUser();
    this.loggedUser = this.authenticationService.currentUserValue;

    this.isAdmin = (this.loggedUser.roleName == Role.Admin) ? true : false ;    
  }

  navigate(str){
    if(str=='edit'){
      this.router.navigate(['/pages/employees/update/'+this.id]);
    }
    if(str=='delete'){
      this.router.navigate(['/pages/employees/delete/'+this.id]);
    }
    if(str=="list"){
      this.router.navigate(['/pages/employees/list']);
    }
  }
}
