export class Employee {
    id: number;
    companyId: number;
    firstName: string;
    lastName: string;
    email: string;
    code: number;
    address1: string;
    address2: string;
    gender: number;
    dateOfBirth: Date;
    languageId: number;
    countryId: number;
    municipalityId: number;
    salutationId: number;
    socialSecurityNo: string;
    electronicDispatch: string;
    lastSalary: number;
    lastSync: Date;
  }