import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-employees',
  template: `<router-outlet></router-outlet>`
})
export class EmployeesComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    //this.router.navigate(['/pages/employees/list']);
  }

}
