import { Component, OnInit } from '@angular/core';
import { Company } from '../company.model';
import { CompanyService } from '../../../services/companyService.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication.service';
import { LoginUser } from '../../../auth/loginUser.model';
import { Role } from '../../ConstsRoles';

@Component({
  selector: 'ngx-company-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.scss']
})
export class ShowComponent implements OnInit {
  company: Company;
  id = this.route.snapshot.params['id'];
  loggedUser: LoginUser;
  isAdmin: boolean;
  
  constructor(private authenticationService: AuthenticationService, private companyService: CompanyService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.company = new Company();
    this.companyService.getCompanyById(this.id)
    .then(res=> this.company = res as Company);

    this.loggedUser = new LoginUser();
    this.loggedUser = this.authenticationService.currentUserValue;

    this.isAdmin = (this.loggedUser.roleName == Role.Admin) ? true : false ;
  }

  navigate(str){
    if(str=='edit'){
      this.router.navigate(['/pages/companies/update/'+this.id]);
    }
    if(str=='delete'){
      this.router.navigate(['/pages/companies/delete/'+this.id]);
    }
    if(str=='employees'){
      this.router.navigate(['/pages/employees/list/'+this.id]);
    }
    if(str=="list"){
      this.router.navigate(['/pages/companies/list']);
    }
  }
}
