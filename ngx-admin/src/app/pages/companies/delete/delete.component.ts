import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { CompanyService } from '../../../services/companyService.service';

@Component({
  selector: 'ngx-company-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {

  constructor(private companyService:CompanyService, private route: ActivatedRoute, private router: Router,private toastrService: NbToastrService) { }

  ngOnInit(): void {
  }

  delete(){
    this.companyService.deleteCompany(this.route.snapshot.params['id']).subscribe( res =>{
      this.toastrService.show("Delete Successfully" ,"Company Delete",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.router.navigate(['/pages/companies/list']);
    },(err: any)=>{
      this.toastrService.show("Something wrong" ,"Company Delete",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    }
    );  
  }
}
