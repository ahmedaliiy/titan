import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompaniesComponent } from './companies.component';
import { AddComponent } from './add/add.component';
import { UpdateComponent } from './update/update.component';
import { DeleteComponent } from './delete/delete.component';
import { ListComponent } from './list/list.component';
import { ShowComponent } from './show/show.component';

const routes: Routes = [{
  path: '',
  component: CompaniesComponent,
  children: [
    {
      path: 'add',
      component: AddComponent,
    },
    {
      path: 'update/:id',
      component: UpdateComponent,
    },
    {
        path: 'delete/:id',
        component: DeleteComponent,
    },
    {
      path: 'list',
      component: ListComponent,
    },
    {
      path: 'show/:id',
      component: ShowComponent,
    }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CompaniesRoutingModule { }

export const routedComponents = [
  CompaniesComponent,
  AddComponent,
  UpdateComponent,
  DeleteComponent,
  ListComponent,
  ShowComponent
];
