import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { Company } from '../company.model';
import { HttpClient } from '@angular/common/http';
import { CompanyService } from '../../../services/companyService.service';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Role } from '../../ConstsRoles';
import { AuthenticationService } from '../../../services/authentication.service';

@Component({
  selector: 'ngx-company-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  companies: Company[] = [];
  userRole: string = '';
  isAuditor: boolean;

  constructor(private as:AuthenticationService, private http: HttpClient,private router: Router,private companyService: CompanyService,private toastrService: NbToastrService) { }

 ngOnInit() {
   this.userRole = this.as.currentUserValue.roleName;
    if(this.userRole == Role.Admin){
      this.companyService.refreshList().then(res => this.companies = res as Company[]);
    }
    if(this.userRole == Role.Auditor){
      this.companyService.GetActiveCompanies().then(res => this.companies = res as Company[]);
    }

    this.isAuditor = (this.as.currentUserValue.roleName == "Auditor") ? true : false;
  }

  navigate(type,id?:number) {
    if(type=='add'){
    this.router.navigate(['/pages/companies/add']);
    }
    if(type=='edit'){
      this.router.navigate(['/pages/companies/update/'+id]);
      }
      if(type=='delete'){
        this.router.navigate(['/pages/companies/delete/'+id]);
        }
        if(type=='show'){
          this.router.navigate(['/pages/companies/show/'+id]);
          }
}

activate(id){
  this.companyService.Activate(id).subscribe(
    res => {
      this.toastrService.show("Activate Successfully" ,"Company Register",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT })
      this.ngOnInit();
    }
  )
}
  deActivate(id){
    this.companyService.Deactivate(id).subscribe(
      res => {
      this.toastrService.show("Deactivate Successfully" ,"Company Register",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT })
      this.ngOnInit();
      }
    )
}
}
