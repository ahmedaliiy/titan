import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ngx-Companies',
  template: `<router-outlet></router-outlet>`
})
export class CompaniesComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.router.navigate(['/pages/companies/list']);
  }

}
