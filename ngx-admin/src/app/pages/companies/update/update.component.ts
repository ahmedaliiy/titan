import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Company } from '../company.model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { CompanyService } from '../../../services/companyService.service';

@Component({
  selector: 'ngx-company-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {
  company: Company;
  id = this.route.snapshot.params['id'];

  userTypes=[
    {
      Id:1,
      Name:'Auditor'
    },
    {
      Id:2,
      Name:'Company User'
    }
  ]

  logo: any;

  constructor(private companyService: CompanyService, private route: ActivatedRoute, private router: Router, private toastrService: NbToastrService) { }

  async ngOnInit() {
    this.company = new Company();    

    await this.companyService.getCompanyById(this.id)
    .then(res=> {
      this.company = res as Company;
      this.company.logo = {
        name: "",
        file: null
      }
    });
  }

  UpdateCompany(f: NgForm){
    f.value.logo = this.company.logo;
    this.companyService.putCompany(f.value).subscribe(res => {
      this.toastrService.show("Update Successfully" ,"Company Update",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.resetForm(f);
      this.router.navigate(['/pages/companies/list']);
    },(err: any)=>{
      this.toastrService.show("Something wrong" ,"Company Update",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    })
  }

  imageError: string;
  isImageSaved: boolean;
  cardImageBase64: string;

  handleInputChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];    
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
    this.company.logo.name = file.name;
  }
  _handleReaderLoaded(e) {
    let reader = e.target;
    this.company.logoPath = reader.result;
    this.company.logo.file = this.company.logoPath;
  }

  resetForm(f?:NgForm){
    if(f != null)
      f.resetForm();
    this.companyService.formData = {
      id: null,
      name: '',
      companyAddress: '',
      cbaRequiredToBeUtilised:0,
      commercialRegistry: '',
      companyRegistrationNumber1:0,
      companyRegistrationNumber2:0,
      email:'',
      employingEntity:'',
      existingPayrollCalendar:'',
      financeSystemFileSpecification:'',
      hrisInUse:null,
      isActive:null,
      isAuthorizedPOARequired:null,
      isCBACurrentlyInPlace:null,
      isPayrollRegistered:null,
      logoPath:'',
      medicalInsuranceNumber:'',
      modifiedMonths:'',
      nameOfCurrentHRIS:'',
      nameOfFinanceSystem:'',
      numberOfFullTimeEmployees:0,
      numberOfHourlyPaidEmployees:0,
      numberOfPartTimeEmployees:0,
      numberOfSalariedEmployees:0,
      payDate: null,
      payFrequency:'',
      payPeriod: '',
      payrollCutOffDate:null,
      payrollName:'',
      payrollResponsibleForPayments:0,
      pensionFundRegistration:'',
      telephoneNumber:'',
      totalPayrollHeadcount:0,
      logo: {
        name: '',
        file: null
      },
      modifyDateTime:null
    }
  }
}
