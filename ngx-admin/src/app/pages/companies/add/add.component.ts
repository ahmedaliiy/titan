import { Component, OnInit } from '@angular/core';
import { Company } from '../company.model';
import { NgForm } from '@angular/forms';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';
import { CompanyService } from '../../../services/companyService.service';

@Component({
  selector: 'ngx-company-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  company: Company;
  logo: any;
  logoFile:any;
  constructor(private router: Router,private companyService: CompanyService,private toastrService: NbToastrService) { }

  ngOnInit(): void {
    this.company = new Company();
    this.company.logo = {
      name: "",
      file: null
    }
    this.company.isActive = false;
    this.company.isPayrollRegistered = false;
    this.company.isAuthorizedPOARequired = false;
    this.company.isCBACurrentlyInPlace = false;
    this.company.hrisInUse = false;

    this.resetForm();

  }

  resetForm(f?:NgForm){
    if(f != null)
      f.resetForm();
    this.companyService.formData = {
      id: null,
      name: '',
      companyAddress: '',
      cbaRequiredToBeUtilised:0,
      commercialRegistry: '',
      companyRegistrationNumber1:0,
      companyRegistrationNumber2:0,
      email:'',
      employingEntity:'',
      existingPayrollCalendar:'',
      financeSystemFileSpecification:'',
      hrisInUse:null,
      isActive:null,
      isAuthorizedPOARequired:null,
      isCBACurrentlyInPlace:null,
      isPayrollRegistered:null,
      logoPath:'',
      medicalInsuranceNumber:'',
      modifiedMonths:'',
      nameOfCurrentHRIS:'',
      nameOfFinanceSystem:'',
      numberOfFullTimeEmployees:0,
      numberOfHourlyPaidEmployees:0,
      numberOfPartTimeEmployees:0,
      numberOfSalariedEmployees:0,
      payDate: null,
      payFrequency:'',
      payPeriod: '',
      payrollCutOffDate:null,
      payrollName:'',
      payrollResponsibleForPayments:0,
      pensionFundRegistration:'',
      telephoneNumber:'',
      totalPayrollHeadcount:0,
      logo: {
        name: '',
        file: null
      },
      modifyDateTime: null
    }
  }

  userTypes=[
    {
      Id:1,
      Name:'Auditor'
    },
    {
      Id:2,
      Name:'Company User'
    }
  ]

  addCompany(f:NgForm){
    f.value.logo = this.company.logo;
    this.companyService.postCompany(f.value).subscribe(res => {
      this.toastrService.show("Insert Successfully" ,"Company Register",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.resetForm(f);
      this.router.navigate(['/pages/companies/list']);
    },(err: any)=>{
      this.toastrService.show("Something wrong" ,"Company Register",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    }
    )
  }

  handleInputChange(e) {
    var file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];    
    var pattern = /image-*/;
    var reader = new FileReader();
    if (!file.type.match(pattern)) {
      alert('invalid format');
      return;
    }
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
    this.company.logo.name = file.name;
  }
  _handleReaderLoaded(e) {
    let reader = e.target;
    this.company.logoPath = reader.result;
    this.company.logo.file = this.company.logoPath;
  }
}
