export class Company {
    id: number;
    name: string;
    logoPath: string;
    companyAddress: string;
    email: string;
    telephoneNumber: string;
    commercialRegistry: string;
    medicalInsuranceNumber: string;
    pensionFundRegistration: string;
    isActive: boolean;
    companyRegistrationNumber1:number;
    companyRegistrationNumber2:number;
    payrollName: string;
    employingEntity: string;
    isPayrollRegistered: boolean;
    isAuthorizedPOARequired: boolean;
    totalPayrollHeadcount: number;
    numberOfFullTimeEmployees: number;
    numberOfPartTimeEmployees: number;
    numberOfSalariedEmployees: number;
    numberOfHourlyPaidEmployees: number;
    payFrequency: string;
    payPeriod: string;
    payrollCutOffDate: Date;
    payDate: Date;
    existingPayrollCalendar: string;
    modifiedMonths: string;
    isCBACurrentlyInPlace: boolean;
    cbaRequiredToBeUtilised: number;
    payrollResponsibleForPayments: number;
    hrisInUse: boolean;
    nameOfCurrentHRIS: string;
    nameOfFinanceSystem: string;
    financeSystemFileSpecification: string;
    logo:{
      name: string,
      file: any
    };
    modifyDateTime: Date;
}