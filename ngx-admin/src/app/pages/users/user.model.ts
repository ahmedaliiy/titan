export class User {
    id: number;
    userName: string;
    roleId: number;
    roleName: string;
    email: string;
    password: string;
    isActive: boolean;
    companyIds: number[];
}