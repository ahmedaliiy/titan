import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { User } from '../user.model';
import { NbToastrService, NbGlobalPhysicalPosition } from '@nebular/theme';
import { UserManagementService } from '../../../services/userManagementService.service';
import { CompanyService } from '../../../services/companyService.service';
import { Company } from '../../companies/company.model';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'ngx-user-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {
  user: User;
  ConfirmPassword:string='';
  id = this.route.snapshot.params['id'];
  companies: Company[];
  comps = [];
  isequal:boolean = false;

  changeStatus(e,value){
    this.isequal = true;
    if(e == value){
      this.isequal = false;
    }
  }
  
  constructor(private companyService: CompanyService,private userService: UserManagementService, private route: ActivatedRoute, private router: Router, private toastrService: NbToastrService) { }

  userTypes=[
    {
      id:2,
      name:'Auditor'
    },
    {
      id:3,
      name:'Company User'
    }
  ]
 


  ngOnInit(): void {
    this.user = new User();
    this.comps = [];
    this.userService.getUserById(this.id)
    .then(res=> {this.user = res as User;});

    this.companyService.GetActiveCompanies().then(res=>this.companies = res as Company[]);
  }

  UpdateUser(f: NgForm){
    if(f.value.roleId == 3){
      this.comps = [];      
      this.comps.push(Number(this.user.companyIds));      
      this.user.companyIds=[];
      this.user.companyIds.push(this.comps[this.comps.length-1]);    
    }
    if(f.value.roleId == 2){
      for(let i =0;i<f.value.companyIds;i++){
        this.user.companyIds = [];
        this.user.companyIds.push(f.value.companyIds[i]);
      }
    } 
    
    this.userService.putUser(this.user).then(res => {
      this.toastrService.show("Update Successfully" ,"User Update",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      //this.resetForm(f);
      this.router.navigate(['/pages/users/list']);
    }).catch(error=>{
      this.toastrService.show("Username is already exists" ,"User Register",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    })
  }

  resetForm(f?:NgForm){
    if(f != null)
      f.resetForm();
    this.userService.formData = {
      id: null,
      companyIds: [],
      email:'',
      isActive:null,
      password:'',
      roleId:0,
      roleName:'',
      userName:''
    }
  }

}

interface ClientError {
  code: string;
  description: string;
}