import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { UserManagementService } from '../../../services/userManagementService.service';

@Component({
  selector: 'ngx-user-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent implements OnInit {

  constructor(private userService:UserManagementService, private route: ActivatedRoute, private router: Router,private toastrService: NbToastrService) { }

  ngOnInit(): void {
  }

  delete(){
    this.userService.deleteUser(this.route.snapshot.params['id']).subscribe( res =>{
      this.toastrService.show("Delete Successfully" ,"User Delete",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.router.navigate(['/pages/users/list']);
    },(err: any)=>{
      this.toastrService.show("Something wrong" ,"User Delete",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    });  
  }
}
