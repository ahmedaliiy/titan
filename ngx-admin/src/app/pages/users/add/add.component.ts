import { Component, OnInit } from '@angular/core';
import { User } from '../user.model';
import { NgForm } from '@angular/forms';
import { Company } from '../../companies/company.model';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';
import { UserManagementService } from '../../../services/userManagementService.service';
import { CompanyService } from '../../../services/companyService.service';

@Component({
  selector: 'ngx-user-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  user: User;
  comps = [];
  ConfirmPassword:string = '';
  companies:Company[]
  isequal:boolean = false;
  userTypes=[
    {
      id:2,
      name:'Auditor'
    },
    {
      id:3,
      name:'Company User'
    }
  ]

  constructor(private router: Router,private userService: UserManagementService,private toastrService: NbToastrService,private companyService : CompanyService) { }

  ngOnInit(): void {
    this.user = new User();
    this.user.isActive = false;

    this.companyService.GetActiveCompanies().then(res=>this.companies = res as Company[]);
  }

  addUser(f: NgForm){
    if(f.value.roleId == 3){
      this.comps = [];      
      this.comps.push(Number(this.user.companyIds));      
      this.user.companyIds=[];
      this.user.companyIds.push(this.comps[this.comps.length-1]);    
    }
    if(f.value.roleId == 2){
      for(let i =0;i<f.value.companyIds;i++){
        this.user.companyIds = [];
        this.user.companyIds.push(f.value.companyIds[i]);
      }
    } 
    
    this.userService.postUser(this.user).then(res => {
      this.toastrService.show("Insert Successfully" ,"User Register",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT });
      this.resetForm(f); 
      this.router.navigate(['/pages/users/list']);
    }).catch(error=>{
      this.toastrService.show("Username is already exists" ,"User Register",{ status: "danger",position: NbGlobalPhysicalPosition.TOP_RIGHT });
    })
  }

  changeStatus(e,value){
    this.isequal = true;
    if(e == value){
      this.isequal = false;
    }
  }

  resetForm(f?:NgForm){
    if(f != null)
      f.resetForm();
    this.userService.formData = {
      id: null,
      companyIds: [],
      email:'',
      isActive:null,
      password:'',
      roleId:0,
      roleName:'',
      userName:''
    }
  }
}