import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { User } from '../user.model';
import { HttpClient } from '@angular/common/http';
import { UserManagementService } from '../../../services/userManagementService.service';
import { NbToastrService, NbGlobalPhysicalPosition } from '@nebular/theme';

@Component({
  selector: 'ngx-user-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  users:User[];
  constructor(private http: HttpClient,private router: Router,private userService: UserManagementService,private toastrService:NbToastrService) { }

  ngOnInit(): void {
    this.userService.refreshList().then(res => this.users = res as User[]);
  }

  navigate(type,id?) {
    if(type=='add'){
    this.router.navigate(['/pages/users/add']);
    }
    if(type=='edit'){
      this.router.navigate(['/pages/users/update/'+id]);
      }
      if(type=='delete'){
        this.router.navigate(['/pages/users/delete/'+id]);
        }
  }

  activate(id){
    this.userService.Activate(id).subscribe(
      res => {
        this.toastrService.show("Activate Successfully" ,"User Register",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT })
        this.ngOnInit();
      }
    )
  }
    deActivate(id){
      this.userService.Deactivate(id).subscribe(
        res => {
        this.toastrService.show("Deactivate Successfully" ,"User Register",{ status: "success",position: NbGlobalPhysicalPosition.TOP_RIGHT })
        this.ngOnInit();
        }
      )
  }
}
