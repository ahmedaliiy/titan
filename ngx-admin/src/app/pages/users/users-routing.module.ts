import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from './users.component';
import { AddComponent } from './add/add.component';
import { UpdateComponent } from './update/update.component';
import { DeleteComponent } from './delete/delete.component';
import { ListComponent } from './list/list.component';

const routes: Routes = [{
  path: '',
  component: UsersComponent,
  children: [
    {
      path: 'add',
      component: AddComponent,
    },
    {
      path: 'update/:id',
      component: UpdateComponent,
    },
    {
        path: 'delete/:id',
        component: DeleteComponent,
    },
    {
      path: 'list',
      component: ListComponent,
  }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule { }

export const routedComponents = [
  UsersComponent,
  AddComponent,
  UpdateComponent,
  DeleteComponent,
  ListComponent
];
