import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Employee } from '../pages/employees/employee.model';
import { Observable } from 'rxjs';
import { ImportModel } from '../pages/employees/import.model';

@Injectable({
    providedIn: 'root'
})

export class EmployeeService{
  formData: Employee;
  employees: Employee[];

  constructor(private http: HttpClient){}

  getEmployeeById(id) { 
    return this.http.get(environment.apiUrl+'/Employee/'+id).toPromise();
  }

  getEmployeeByCompany(id) { 
    return this.http.get(environment.apiUrl+'/Employee/GetByCompany/'+id).toPromise();
  }

  postEmployee(formData: Employee){
    return this.http.post(environment.apiUrl+'/Employee',formData);
  }

  putEmployee(formData: Employee){
    return this.http.put(environment.apiUrl+'/Employee/'+formData.id,formData);
  }

  deleteEmployee(id){
    return this.http.delete(environment.apiUrl+'/Employee/'+id);
  }

  refreshList(){
    return this.http.get(environment.apiUrl + '/Employee').toPromise();
  }

  syncEmployee(id){
    return this.http.get(environment.apiUrl + '/Employee/Sync/'+id);
  }

  downloadTemplate(){
    return this.http.get(environment.apiUrl + '/Employee/employee-template-path',{responseType: 'text'});
  }

  importFile(importModel: ImportModel){
    return this.http.post(environment.apiUrl+'/Employee/import-employee',importModel).toPromise();
  }
}