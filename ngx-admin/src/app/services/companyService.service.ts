import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Company } from '../pages/companies/company.model';

@Injectable({
    providedIn: 'root'
})

export class CompanyService{
  formData: Company;
  countries: Company[];

  constructor(private http: HttpClient){}

  getCompanyById(id) { 
    return this.http.get(environment.apiUrl+'/Company/'+id).toPromise();
  }

  GetActiveCompanies(){
    return this.http.get(environment.apiUrl + '/Company/GetActiveCompanies').toPromise();
  }

  Activate(id){
    return this.http.get(environment.apiUrl+'/Company/Activate/'+id);
  }

  Deactivate(id){
    return this.http.get(environment.apiUrl+'/Company/Deactivate/'+id);
  }

  postCompany(formData: Company){
    return this.http.post(environment.apiUrl+'/Company',formData);
  }

  putCompany(formData: Company){
    return this.http.put(environment.apiUrl+'/Company/'+formData.id,formData);
  }

  deleteCompany(id){
    return this.http.delete(environment.apiUrl+'/Company/'+id);
  }

  refreshList(){
    return this.http.get(environment.apiUrl + '/Company').toPromise();
  }

}