import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Language } from '../pages/languages/language.model';

@Injectable({
    providedIn: 'root'
})

export class LanguageService{
  formData: Language;
  countries: Language[];

  constructor(private http: HttpClient){}

  getLanguageById(id) { 
    return this.http.get(environment.apiUrl+'/Language/'+id).toPromise();
  }

  postLanguage(formData: Language){
    return this.http.post(environment.apiUrl+'/Language',formData);
  }

  putLanguage(formData: Language){
    return this.http.put(environment.apiUrl+'/Language/'+formData.id,formData);
  }

  deleteLanguage(id){
    return this.http.delete(environment.apiUrl+'/Language/'+id);
  }

  refreshList(){
    return this.http.get(environment.apiUrl + '/Language').toPromise();
  }

}