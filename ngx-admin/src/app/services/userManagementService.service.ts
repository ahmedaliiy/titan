import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { User } from '../pages/users/user.model';

@Injectable({
    providedIn: 'root'
})

export class UserManagementService{
  formData: User;
  countries: User[];

  constructor(private http: HttpClient){}

  getUserById(id) { 
    return this.http.get(environment.apiUrl+'/user-management/user/'+id).toPromise();
  }

  GetActiveCompanies(){
    return this.http.get(environment.apiUrl + '/Company/GetActiveCompanies').toPromise();
  }

  Activate(id){
    return this.http.get(environment.apiUrl+'/user-management/Activate/'+id);
  }

  Deactivate(id){
    return this.http.get(environment.apiUrl+'/user-management/Deactivate/'+id);
  }

  postUser(formData: User){
    return this.http.post(environment.apiUrl+'/user-management/user',formData).toPromise();
  }

  putUser(formData: User){
    return this.http.put(environment.apiUrl+'/user-management/user',formData).toPromise();
  }

  deleteUser(id){
    return this.http.delete(environment.apiUrl+'/user-management/user/'+id);
  }

  refreshList(){
    return this.http.get(environment.apiUrl + '/user-management/users').toPromise();
  }

}