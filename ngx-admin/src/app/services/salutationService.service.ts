import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Salutation } from '../pages/salutations/salutations.model';

@Injectable({
    providedIn: 'root'
})

export class SalutationService{
  formData: Salutation;
  countries: Salutation[];

  constructor(private http: HttpClient){}

  getSalutationById(id) { 
    return this.http.get(environment.apiUrl+'/Salutation/'+id).toPromise();
  }

  postSalutation(formData: Salutation){
    return this.http.post(environment.apiUrl+'/Salutation',formData);
  }

  putSalutation(formData: Salutation){
    return this.http.put(environment.apiUrl+'/Salutation/'+formData.id,formData);
  }

  deleteSalutation(id){
    return this.http.delete(environment.apiUrl+'/Salutation/'+id);
  }

  refreshList(){
    return this.http.get(environment.apiUrl + '/Salutation').toPromise();
  }

}