import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Municipality } from '../pages/municipalities/municipalities.model';

@Injectable({
    providedIn: 'root'
})

export class MunicipalityService{
  formData: Municipality;
  countries: Municipality[];

  constructor(private http: HttpClient){}

  getMunicipalityById(id) { 
    return this.http.get(environment.apiUrl+'/Municipality/'+id).toPromise();
  }

  postMunicipality(formData: Municipality){
    return this.http.post(environment.apiUrl+'/Municipality',formData);
  }

  putMunicipality(formData: Municipality){
    return this.http.put(environment.apiUrl+'/Municipality/'+formData.id,formData);
  }

  deleteMunicipality(id){
    return this.http.delete(environment.apiUrl+'/Municipality/'+id);
  }

  refreshList(){
    return this.http.get(environment.apiUrl + '/Municipality').toPromise();
  }

}