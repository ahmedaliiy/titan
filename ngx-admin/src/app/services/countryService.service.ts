import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Country } from '../pages/countries/country.model';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})

export class CountryService{
  formData: Country;
  countries: Country[];

  constructor(private http: HttpClient){}

  getCountryById(id) { 
    return this.http.get(environment.apiUrl+'/Country/'+id).toPromise();
  }

  postCountry(formData: Country){
    return this.http.post(environment.apiUrl+'/Country',formData);
  }

  putCountry(formData: Country){
    return this.http.put(environment.apiUrl+'/Country/'+formData.id,formData);
  }

  deleteCountry(id){
    return this.http.delete(environment.apiUrl+'/Country/'+id);
  }

  refreshList(){
    return this.http.get(environment.apiUrl + '/Country').toPromise();
  }

}