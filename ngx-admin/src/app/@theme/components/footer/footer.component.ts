import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">
      Created with <b><a href="https://www.orchtech.com/" target="_blank">Orchtech</a></b> 2020
    </span>
    <div class="socials">
      <a href="https://www.facebook.com/orchtechsoftwarehouse" target="_blank" class="ion ion-social-facebook"></a>
      <a href="https://www.linkedin.com/company/orchtech?trk=company_logo" target="_blank" class="ion ion-social-linkedin"></a>
    </div>
  `,
})
export class FooterComponent {
}
