import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { NbLoginComponent, NbAuthService } from '@nebular/auth';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { first } from 'rxjs/operators';
import { NbToastrService, NbGlobalPhysicalPosition } from '@nebular/theme';
import { Role } from '../../pages/ConstsRoles';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
})
export class NgxLoginComponent implements OnInit {
  validInputs: boolean = true;
  name:string = '';
  password:string = '';
  returnUrl: string;


  constructor(private route: ActivatedRoute, private authenticationService: AuthenticationService, private router:Router)
  {
  }

  ngOnInit(){
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  dologin() {   
    this.authenticationService.login(this.name, this.password)
        .pipe(first())
        .subscribe(
            data => {
              this.router.navigate([this.returnUrl]);
            },
            (err: any)=>{
              this.validInputs = false;
              });
}
}