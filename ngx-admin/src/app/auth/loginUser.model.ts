import { Role } from '../pages/ConstsRoles';

export class LoginUser {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    token: string;
    roleName: Role;
}